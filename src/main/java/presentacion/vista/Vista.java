package presentacion.vista;

import dto.PersonaDTO;
import persistencia.conexion.Conexion;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

public class Vista
{
	private JFrame frmAgenda;
	private JTable tablaPersonas;
	private JButton btnAgregar;
	private JButton btnBorrar;
	private JButton btnReporte;
	private JButton btnNuevoTipoContacto;
	private JButton btnLocalidadAbm;
	private JButton btnEditar;
	private DefaultTableModel modelPersonas;
	private String[] nombreColumnas = {"IdPersona","Nombre y apellido","Telefono","Email","Fecha Nacimiento","Tipo de Contacto", "calle", "altura", "piso", "dpto", "localidad", "LinkedIn", "Codigo Postal"};


	public Vista() 
	{
		super();
		initialize();
	}


	private void initialize() 
	{
		frmAgenda = new JFrame();
		frmAgenda.setTitle("Agenda");
		frmAgenda.setBounds(100, 100, 1257, 400);
		frmAgenda.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmAgenda.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 1225, 350);
		frmAgenda.getContentPane().add(panel);
		panel.setLayout(null);
		
		JScrollPane spPersonas = new JScrollPane();
		spPersonas.setEnabled(false);
		spPersonas.setBounds(10, 11, 1205, 182);
		panel.add(spPersonas);
		
		modelPersonas = new DefaultTableModel(null,nombreColumnas);
		tablaPersonas = new JTable(modelPersonas);
		
		
		tablaPersonas.getColumnModel().getColumn(0).setPreferredWidth(103);
		tablaPersonas.getColumnModel().getColumn(0).setResizable(false);
		tablaPersonas.getColumnModel().getColumn(1).setPreferredWidth(100);
		tablaPersonas.getColumnModel().getColumn(1).setResizable(false);
		tablaPersonas.setAutoCreateRowSorter(true);
		
		
		
		spPersonas.setViewportView(tablaPersonas);
		
		btnAgregar = new JButton("Agregar");
		btnAgregar.setBounds(397, 204, 123, 39);
		panel.add(btnAgregar);
		
		btnEditar = new JButton("Editar");
		btnEditar.setBounds(643, 204, 123, 39);
		panel.add(btnEditar);
		
		btnBorrar = new JButton("Borrar");
		btnBorrar.setBounds(643, 265, 123, 39);
		panel.add(btnBorrar);
		
		btnReporte = new JButton("Reporte");
		btnReporte.setBounds(397, 265, 123, 39);
		panel.add(btnReporte);
		
		btnNuevoTipoContacto = new JButton("Tipo Contacto - ABM");
		btnNuevoTipoContacto.setBounds(10, 300, 165, 39);
		panel.add(btnNuevoTipoContacto);
		
		btnLocalidadAbm = new JButton("Localidad - ABM");
		btnLocalidadAbm.setBounds(1059, 300, 156, 39);
		panel.add(btnLocalidadAbm);
	}

	public void show()
	{
		this.frmAgenda.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.frmAgenda.addWindowListener(new WindowAdapter() 
		{
			@Override
		    public void windowClosing(WindowEvent e) {
		        int confirm = JOptionPane.showOptionDialog(
		             null, "¿Estás seguro que quieres salir de la Agenda?", 
		             "Confirmación", JOptionPane.YES_NO_OPTION,
		             JOptionPane.QUESTION_MESSAGE, null, null, null);
		        if (confirm == 0) {
		        	Conexion.getConexion().cerrarConexion();
		           System.exit(0);
		        }
		    }
		});
		this.frmAgenda.setVisible(true);
	}
	
	public JButton getBtnAgregar() 
	{
		return btnAgregar;
	}

	public JButton getBtnBorrar() 
	{
		return btnBorrar;
	}
	
	public JButton getBtnReporte() 
	{
		return btnReporte;
	}
	
	public JButton getBtnNuevoTipoContacto()
	{
		return btnNuevoTipoContacto;
	}
	
	public DefaultTableModel getModelPersonas() 
	{
		return modelPersonas;
	}
	
	public JTable getTablaPersonas()
	{
		return tablaPersonas;
	}

	public String[] getNombreColumnas() 
	{
		return nombreColumnas;
	}
	
	public JButton getBtnLocalidadAbm() {
		return btnLocalidadAbm;
	}

	public void setBtnLocalidadAbm(JButton btnLocalidadAbm) {
		this.btnLocalidadAbm = btnLocalidadAbm;
	}

	public JButton getBtnEditar() {
		return btnEditar;
	}

	public void setBtnEditar(JButton btnEditar) {
		this.btnEditar = btnEditar;
	}
	

	public void llenarTabla(List<PersonaDTO> personasEnTabla) {
		this.getModelPersonas().setRowCount(0); //Para vaciar la tabla
		this.getModelPersonas().setColumnCount(0);
		this.getModelPersonas().setColumnIdentifiers(this.getNombreColumnas());

		for (PersonaDTO p : personasEnTabla)
		{
			int idPersona = p.getIdPersona();
			String nombre = p.getNombre();
			String tel = p.getTelefono();
			String email = p.getEmail();
			String fecha_nac = p.getFecha();
			String tipoContacto = p.getTipoContacto();
			String calle = p.getCalle();
			String altura = p.getAltura();
			String piso = p.getPiso();
			String dpto = p.getDpto();
			String localidad = p.getLocalidad();
			String linkedin = p.getLinkedin();
			String cp = p.getCp();
			Object[] fila = {idPersona, nombre, tel,email,fecha_nac,tipoContacto,calle,altura,piso,dpto,localidad, linkedin, cp};
			this.getModelPersonas().addRow(fila);
		}
		
	}
}
