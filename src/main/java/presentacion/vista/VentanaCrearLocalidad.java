package presentacion.vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import dto.LocalidadDTO;
import dto.PaisDTO;
import dto.TipoContactoDTO;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;

public class VentanaCrearLocalidad extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JButton btnNuevaLocalidad;
	private static VentanaCrearLocalidad INSTANCE;
	private DefaultTableModel modelLocalidad;
	private  String[] nombreColumnas = {"Id","Localidad","Provincia","Pais"};
	private JLabel lblPais;
	private JComboBox<String> comboBoxPais;
	private JLabel lblProvincias;
	private JComboBox<String> comboBoxProvincia;
	private JTextField textFieldLocalidad;
	private JLabel lblLocalidad;
	
	public static VentanaCrearLocalidad getInstance() 
	{
		
		if (INSTANCE == null) 
		{
			INSTANCE = new VentanaCrearLocalidad();
			return new VentanaCrearLocalidad();
		}
		else
			return INSTANCE;
	}

	/**
	 * Create the frame.
	 */
	public VentanaCrearLocalidad() 
	{
		super();
		setTitle("Nueva Localidad");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 311, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);		
		
		JPanel panel = new JPanel();
		panel.setBounds(20, 20, 606, 236);
		contentPane.add(panel);
		panel.setLayout(null);
		
		modelLocalidad = new DefaultTableModel(null,nombreColumnas);
		
		btnNuevaLocalidad = new JButton("Guardar");
		btnNuevaLocalidad.setBounds(43, 188, 186, 36);
		panel.add(btnNuevaLocalidad);
		
		lblPais = new JLabel("Pais: ");
		lblPais.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblPais.setBounds(32, 38, 92, 14);
		panel.add(lblPais);
		
		comboBoxPais = new JComboBox<String>();
		comboBoxPais.setBounds(134, 35, 111, 20);
		panel.add(comboBoxPais);
		
		lblProvincias = new JLabel("Provincias: ");
		lblProvincias.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblProvincias.setBounds(32, 66, 92, 14);
		panel.add(lblProvincias);
		
		comboBoxProvincia = new JComboBox<String>();
		comboBoxProvincia.setBounds(134, 63, 111, 20);
		panel.add(comboBoxProvincia);
		
		textFieldLocalidad = new JTextField();
		textFieldLocalidad.setBounds(134, 94, 111, 20);
		panel.add(textFieldLocalidad);
		textFieldLocalidad.setColumns(10);
		
		lblLocalidad = new JLabel("Localidad");
		lblLocalidad.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblLocalidad.setBounds(32, 93, 92, 17);
		panel.add(lblLocalidad);
		
		this.setVisible(false);
		}
	
	public void mostrarVentana() 
		{
			this.setVisible(true);
		}
	
	public DefaultTableModel getModelLocalidad() {
		return modelLocalidad;
	}

	public void setModelLocalidad(DefaultTableModel modelLocalidad) {
		this.modelLocalidad = modelLocalidad;
	}
	

	public String[] getNombreColumnas() {
		return nombreColumnas;
	}

	public void setNombreColumnas(String[] nombreColumnas) {
		this.nombreColumnas = nombreColumnas;
	}
	

	public JButton getBtnNuevaLocalidad() {
		return btnNuevaLocalidad;
	}

	public void setBtnNuevaLocalidad(JButton btnAgregarLocalidad) {
		this.btnNuevaLocalidad = btnAgregarLocalidad;
	}
	
	public JComboBox<String> getComboBoxPais() {
		return comboBoxPais;
	}
	
	public String getComboPais()
	{
		return comboBoxPais.getSelectedItem().toString();
	}

	public void setComboBoxPais(JComboBox<String> comboBoxPais) {
		this.comboBoxPais = comboBoxPais;
	}
	
	public JComboBox<String> getComboBoxProvincia() {
		return comboBoxProvincia;
	}

	public void setComboBoxProvincia(JComboBox<String> comboBoxProvincia) {
		this.comboBoxProvincia = comboBoxProvincia;
	}
	
	public String getComboProvincia()
	{
		return comboBoxProvincia.getSelectedItem().toString();
	}

	public JTextField getTextFieldLocalidad() {
		return textFieldLocalidad;
	}

	public void setTextFieldLocalidad(JTextField textFieldLocalidad) {
		this.textFieldLocalidad = textFieldLocalidad;
	}

	public void llenarTabla(List<LocalidadDTO> localidadesEnTabla) 
	{
		this.getModelLocalidad().setRowCount(0); //Para vaciar la tabla
		this.getModelLocalidad().setColumnCount(0);
		this.getModelLocalidad().setColumnIdentifiers(this.getNombreColumnas());

		for (LocalidadDTO p : localidadesEnTabla)
		{
			int id = p.getIdLocalidad();
			String nombre = p.getNombre();
			String provincia = p.getProvincia();
			String pais = p.getPais();
			Object[] fila = {id,nombre,provincia,pais};
			this.getModelLocalidad().addRow(fila);
		}
		
	}
	
	public void agregarElementoAComboPais(List<PaisDTO> paisesEnTabla)
	{
		//comboBoxPais.removeAllItems();
		for (int i = 0; i < paisesEnTabla.size(); i++)
		{
			comboBoxPais.addItem(paisesEnTabla.get(i).getNombre());
		}
	}
	
	

	public void agregarElementoAComboProvincia(List<String> provincias_a_mostrar) {
		comboBoxProvincia.removeAllItems();
		for (int i = 0; i < provincias_a_mostrar.size(); i++)
		{
			comboBoxProvincia.addItem(provincias_a_mostrar.get(i));
		}
	}
	

	public void cerrar()
	{
		this.textFieldLocalidad.setText(null);
		this.dispose();
	}
}
