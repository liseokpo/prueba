package presentacion.vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import dto.LocalidadDTO;
import dto.TipoContactoDTO;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class VentanaLocalidad extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JButton btnNuevaLocalidad;
	private JButton btnBorrar;
	private JButton btnEditar;
	private JButton btnAgregarPais;
	private JButton btnAgregarProvincia;
	private static VentanaLocalidad INSTANCE;
	private DefaultTableModel modelLocalidad;
	private JTable tablaLocalidades;
	private  String[] nombreColumnas = {"Id","Localidad","Provincia","Pais"};
	
	public static VentanaLocalidad getInstance() 
	{
		
		if (INSTANCE == null) 
		{
			INSTANCE = new VentanaLocalidad();
			return new VentanaLocalidad();
		}
		else
			return INSTANCE;
	}

	/**
	 * Create the frame.
	 */
	public VentanaLocalidad() 
	{
		super();
		setTitle("Nueva Localidad");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 659, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);		
		
		JPanel panel = new JPanel();
		panel.setBounds(20, 20, 606, 236);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JScrollPane spTiposContactos = new JScrollPane();
		spTiposContactos.setBounds(10, 11, 390, 182);
		panel.add(spTiposContactos);
		
		modelLocalidad = new DefaultTableModel(null,nombreColumnas);
		tablaLocalidades = new JTable(modelLocalidad);
		
		spTiposContactos.setViewportView(tablaLocalidades);
		
		btnEditar = new JButton("Editar");
		btnEditar.setBounds(65, 204, 111, 36);
		panel.add(btnEditar);
		
		btnBorrar = new JButton("Borrar");
		btnBorrar.setBounds(235, 204, 111, 36);
		panel.add(btnBorrar);
		
		btnNuevaLocalidad = new JButton("Nueva Localidad");
		btnNuevaLocalidad.setBounds(437, 120, 186, 36);
		panel.add(btnNuevaLocalidad);
		
		btnAgregarPais = new JButton("Agregar Pais");
		btnAgregarPais.setBounds(437, 27, 186, 36);
		panel.add(btnAgregarPais);
		
		btnAgregarProvincia = new JButton("Agregar Provincia");
		btnAgregarProvincia.setBounds(437, 74, 186, 36);
		panel.add(btnAgregarProvincia);
		
		this.setVisible(false);
		}
	
	public void mostrarVentana() 
		{
			this.setVisible(true);
		}
	
	public DefaultTableModel getModelLocalidad() {
		return modelLocalidad;
	}

	public void setModelLocalidad(DefaultTableModel modelLocalidad) {
		this.modelLocalidad = modelLocalidad;
	}
	

	public String[] getNombreColumnas() {
		return nombreColumnas;
	}

	public void setNombreColumnas(String[] nombreColumnas) {
		this.nombreColumnas = nombreColumnas;
	}
	
	public JTable getTablaLocalidades() {
		return tablaLocalidades;
	}

	public void setTablaLocalidades(JTable tablaLocalidades) {
		this.tablaLocalidades = tablaLocalidades;
	}

	public JButton getBtnBorrar() {
		return btnBorrar;
	}

	public void setBtnBorrar(JButton btnBorrar) {
		this.btnBorrar = btnBorrar;
	}

	public JButton getBtnNuevaLocalidad() {
		return btnNuevaLocalidad;
	}

	public void setBtnNuevaLocalidad(JButton btnAgregarLocalidad) {
		this.btnNuevaLocalidad = btnAgregarLocalidad;
	}

	public JButton getBtnEditar() {
		return btnEditar;
	}

	public void setBtnEditar(JButton btnEditar) {
		this.btnEditar = btnEditar;
	}
	
	public JButton getBtnAgregarPais() {
		return btnAgregarPais;
	}

	public void setBtnAgregarPais(JButton btnAgregarPais) {
		this.btnAgregarPais = btnAgregarPais;
	}

	public JButton getBtnAgregarProvincia() {
		return btnAgregarProvincia;
	}

	public void setBtnAgregarProvincia(JButton btnAgregarProvincia) {
		this.btnAgregarProvincia = btnAgregarProvincia;
	}

	public void llenarTabla(List<LocalidadDTO> localidadesEnTabla) 
	{
		this.getModelLocalidad().setRowCount(0); //Para vaciar la tabla
		this.getModelLocalidad().setColumnCount(0);
		this.getModelLocalidad().setColumnIdentifiers(this.getNombreColumnas());

		for (LocalidadDTO p : localidadesEnTabla)
		{
			int id = p.getIdLocalidad();
			String nombre = p.getNombre();
			String provincia = p.getProvincia();
			String pais = p.getPais();
			Object[] fila = {id,nombre,provincia,pais};
			this.getModelLocalidad().addRow(fila);
		}
		
	}
	
	public void cerrar()
	{
		
		this.dispose();
	}
}
