package presentacion.vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import dto.LocalidadDTO;
import dto.TipoContactoDTO;

public class VentanaEditarPersona extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtNombre;
	private JTextField txtTelefono;
	private JTextField txtEmail;
	private JComboBox<String> comboTipoContacto;
	private JComboBox<String> comboLocalidades;
	private JTextField txtFechaNac;
	private static VentanaEditarPersona INSTANCE;
	private JTextField textFieldCalle;
	private JTextField textFieldAltura;
	private JTextField textFieldPiso;
	private JTextField textFieldDpto;
	private JTextField txtFieldLinkedin;
	private JTextField txtFieldCp;
	private JButton btnActualizar;
	
	

	public static VentanaEditarPersona getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaEditarPersona(); 	
			return new VentanaEditarPersona();
		}
		else
			return INSTANCE;
	}
	
	public VentanaEditarPersona() {
		super();
		setTitle("Editar Contacto");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 200, 458, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 414, 439);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNombreYApellido = new JLabel("Nombre y apellido");
		lblNombreYApellido.setBounds(10, 11, 113, 14);
		panel.add(lblNombreYApellido);
		
		JLabel lblTelfono = new JLabel("Telefono");
		lblTelfono.setBounds(10, 36, 113, 14);
		panel.add(lblTelfono);
		
		JLabel lblFechaNac = new JLabel("Fecha de nacimiento (AAAA-MM-DD)");
		lblFechaNac.setBounds(10, 61, 250, 14);
		panel.add(lblFechaNac);
		
	
		JLabel lblDomicilio = new JLabel("Domicilio");
		lblDomicilio.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblDomicilio.setBounds(10, 111, 113, 14);
		panel.add(lblDomicilio);
		
		JLabel lblCalle = new JLabel("Calle: ");
		lblCalle.setBounds(53, 136, 46, 14);
		panel.add(lblCalle);
		
		JLabel lblAltura = new JLabel("Altura: ");
		lblAltura.setBounds(53, 161, 46, 14);
		panel.add(lblAltura);
		
		JLabel lblPiso = new JLabel("Piso: ");
		lblPiso.setBounds(53, 186, 46, 14);
		panel.add(lblPiso);
		
		JLabel lblDpto = new JLabel("Dpto: ");
		lblDpto.setBounds(53, 211, 46, 14);
		panel.add(lblDpto);
		
		JLabel lblLocalidad = new JLabel("Localidad");
		lblLocalidad.setBounds(53, 236, 70, 14);
		panel.add(lblLocalidad);

		JLabel lblCp = new JLabel("Codigo Postal");
		lblCp.setBounds(10, 261, 100, 14);
		panel.add(lblCp);
		
		JLabel lblEmail = new JLabel("E-mail");
		lblEmail.setBounds(10, 286, 46, 14);
		panel.add(lblEmail);
		
		JLabel lblLinkedin = new JLabel("Linkedin");
		lblLinkedin.setBounds(10, 311, 100, 14);
		panel.add(lblLinkedin);
		
		JLabel lblTipoContacto = new JLabel("Tipo Contacto");
		lblTipoContacto.setBounds(10, 336, 100, 14);
		panel.add(lblTipoContacto);
		
		txtNombre = new JTextField();
		txtNombre.setBounds(133, 11, 224, 20);
		panel.add(txtNombre);
		txtNombre.setColumns(10);
		
		txtTelefono = new JTextField();
		txtTelefono.setBounds(133, 36, 224, 20);
		panel.add(txtTelefono);
		txtTelefono.setColumns(10);
		
		txtFechaNac = new JTextField();
		txtFechaNac.setText("2020-03-01");
		txtFechaNac.setToolTipText("");
		txtFechaNac.setBounds(133, 86, 224, 20);
		panel.add(txtFechaNac);		
		
		textFieldCalle = new JTextField();
		textFieldCalle.setBounds(132, 136, 225, 20);
		panel.add(textFieldCalle);
		textFieldCalle.setColumns(10);
		
		textFieldAltura = new JTextField();
		textFieldAltura.setBounds(133, 161, 224, 20);
		panel.add(textFieldAltura);
		textFieldAltura.setColumns(10);
		
		textFieldPiso = new JTextField();
		textFieldPiso.setText("-");
		textFieldPiso.setBounds(133, 186, 224, 20);
		panel.add(textFieldPiso);
		textFieldPiso.setColumns(10);

		textFieldDpto = new JTextField();
		textFieldDpto.setText("-");
		textFieldDpto.setBounds(133, 211, 224, 20);
		panel.add(textFieldDpto);
		textFieldDpto.setColumns(10);
		
		comboLocalidades = new JComboBox<String>();
		comboLocalidades.setBounds(133, 236, 224, 20);
		panel.add(comboLocalidades);
		
		txtFieldCp = new JTextField();
		txtFieldCp.setBounds(133, 261, 224, 20);
		panel.add(txtFieldCp);
		
		txtEmail = new JTextField();
		txtEmail.setBounds(133, 286, 224, 20);
		panel.add(txtEmail);
		
		txtFieldLinkedin = new JTextField();
		txtFieldLinkedin.setBounds(133, 311, 224, 20);
		panel.add(txtFieldLinkedin);
		
		comboTipoContacto = new JComboBox<String>();
		comboTipoContacto.setBounds(133, 336, 224, 20);
		panel.add(comboTipoContacto);
		
		btnActualizar = new JButton("Actualizar");
		btnActualizar.setBounds(133, 391, 139, 37);
		panel.add(btnActualizar);
		
		this.setVisible(false);
	}
	
	public void mostrarVentana()
	{
		this.setVisible(true);
	}
	
	
	public JButton getBtnActualizar() {
		return btnActualizar;
	}

	public void setBtnActualizar(JButton btnActualizar) {
		this.btnActualizar = btnActualizar;
	}
	
	public JTextField getTxtNombre() {
		return txtNombre;
	}

	public void setTxtNombre(JTextField txtNombre) {
		this.txtNombre = txtNombre;
	}

	public JTextField getTxtTelefono() {
		return txtTelefono;
	}

	public void setTxtTelefono(JTextField txtTelefono) {
		this.txtTelefono = txtTelefono;
	}

	public JTextField getTxtEmail() {
		return txtEmail;
	}

	public void setTxtEmail(JTextField txtEmail) {
		this.txtEmail = txtEmail;
	}

	public JTextField getTxtFechaNac() {
		return txtFechaNac;
	}

	public void setTxtFechaNac(JTextField txtFechaNac) {
		this.txtFechaNac = txtFechaNac;
	}

	public JTextField getTextFieldCalle() {
		return textFieldCalle;
	}

	public void setTextFieldCalle(JTextField textFieldCalle) {
		this.textFieldCalle = textFieldCalle;
	}

	public JTextField getTextFieldAltura() {
		return textFieldAltura;
	}

	public void setTextFieldAltura(JTextField textFieldAltura) {
		this.textFieldAltura = textFieldAltura;
	}

	public JTextField getTextFieldPiso() {
		return textFieldPiso;
	}

	public void setTextFieldPiso(JTextField textFieldPiso) {
		this.textFieldPiso = textFieldPiso;
	}

	public JTextField getTextFieldDpto() {
		return textFieldDpto;
	}

	public void setTextFieldDpto(JTextField textFieldDpto) {
		this.textFieldDpto = textFieldDpto;
	}
	
	public String getComboTipoContacto() {
		return  comboTipoContacto.getSelectedItem().toString();
	}

	public void setComboTipoContacto(JComboBox<String> comboTipoContacto) {
		this.comboTipoContacto = comboTipoContacto;
	}

	public String getComboLocalidades() {
		return comboLocalidades.getSelectedItem().toString();
	}

	public void setComboLocalidades(JComboBox<String> comboLocalidades) {
		this.comboLocalidades = comboLocalidades;
	}

	
	public JTextField getTxtLinkedin() {
		return txtFieldLinkedin;
	}

	public void setTxtLinkedin(JTextField txtFieldLinkedin) {
		this.txtFieldLinkedin = txtFieldLinkedin;
	}
	
	public JTextField getTxtCp() {
		return txtFieldCp;
	}

	public void setTxtCp(JTextField txtFieldCp) {
		this.txtFieldCp = txtFieldCp;
	}
	
	
	
	public void cerrar()
	{
		this.dispose();
	}

	public void AgregarElementosAComboTipoContacto(List<TipoContactoDTO> tipoContactosEnTabla)
	{
		comboTipoContacto.removeAllItems();
		for (int i = 0; i < tipoContactosEnTabla.size(); i++)
		{
			comboTipoContacto.addItem(tipoContactosEnTabla.get(i).getNombre());
		}
	}
	
	public void AgregarElementoAComboLocalidad(List<LocalidadDTO> localidadesEnTabla)
	{
		
		comboLocalidades.removeAllItems();
		for (int i = 0; i < localidadesEnTabla.size(); i++)
		{
			comboLocalidades.addItem(localidadesEnTabla.get(i).getNombre() + " - " + localidadesEnTabla.get(i).getProvincia() + " - " + localidadesEnTabla.get(i).getPais());
		}
	}

}
