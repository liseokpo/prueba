package presentacion.vista;


import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import java.awt.Font;

public class VentanaEditarTipoContacto extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtNombre;
	private JButton btnAceptar;
	private static VentanaEditarTipoContacto INSTANCE;
	

	public static VentanaEditarTipoContacto getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaEditarTipoContacto(); 	
			return new VentanaEditarTipoContacto();
		}
		else
			return INSTANCE;
	}

	private VentanaEditarTipoContacto() 
	{
		super();
		setTitle("Editar Tipo Contacto");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 200, 376, 132);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 340, 82);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNombre = new JLabel("Nuevo nombre:");
		lblNombre.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNombre.setBounds(10, 11, 113, 14);
		panel.add(lblNombre);
		
		txtNombre = new JTextField();
		txtNombre.setBounds(133, 8, 197, 20);
		panel.add(txtNombre);
		txtNombre.setColumns(10);
	
		btnAceptar = new JButton("Guardar");
		btnAceptar.setBounds(10, 36, 97, 35);
		panel.add(btnAceptar);
		
		this.setVisible(false);
	}
	
	public void mostrarVentana()
	{
		this.setVisible(true);
	}
	
	public JTextField getTxtNombre() 
	{
		return txtNombre;
	}

	public JButton getBtnAceptar() {
		return btnAceptar;
	}

	public void setBtnAceptar(JButton btnAceptar) {
		this.btnAceptar = btnAceptar;
	}

	public void cerrar()
	{
		this.txtNombre.setText(null);
		this.dispose();
	}
}
