package presentacion.vista;

import dto.PaisDTO;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.List;

public class VentanaAbmPais extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JButton btnBorrar;
	private JButton btnAgregarPais;
	private static VentanaAbmPais INSTANCE;
	private DefaultTableModel modelLocalidad;
	private JTable tablaPaises;
	private  String[] nombreColumnas = {"Id","Pais"};
	private JTextField textNuevoPais;
	
	public static VentanaAbmPais getInstance() 
	{
		
		if (INSTANCE == null) 
		{
			INSTANCE = new VentanaAbmPais();
			return new VentanaAbmPais();
		}
		else
			return INSTANCE;
	}

	/**
	 * Create the frame.
	 */
	public VentanaAbmPais() 
	{
		super();
		setTitle("Nuevo pais");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 659, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);		
		
		JPanel panel = new JPanel();
		panel.setBounds(20, 20, 606, 236);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JScrollPane spTiposContactos = new JScrollPane();
		spTiposContactos.setBounds(10, 11, 390, 182);
		panel.add(spTiposContactos);
		
		modelLocalidad = new DefaultTableModel(null,nombreColumnas);
		tablaPaises = new JTable(modelLocalidad);
		
		spTiposContactos.setViewportView(tablaPaises);
		
		btnBorrar = new JButton("Borrar");
		btnBorrar.setBounds(122, 204, 111, 36);
		panel.add(btnBorrar);
		
		btnAgregarPais = new JButton("Agregar Pais");
		btnAgregarPais.setBounds(451, 95, 129, 36);
		panel.add(btnAgregarPais);
		
		JLabel lblNuevoPais = new JLabel("Nuevo Pais: ");
		lblNuevoPais.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNuevoPais.setBounds(410, 39, 78, 14);
		panel.add(lblNuevoPais);
		
		textNuevoPais = new JTextField();
		textNuevoPais.setBounds(498, 36, 121, 20);
		panel.add(textNuevoPais);
		textNuevoPais.setColumns(10);
		
		this.setVisible(false);
		}
	
	public void mostrarVentana() 
		{
			this.setVisible(true);
		}
	
	public DefaultTableModel getModelLocalidad() {
		return modelLocalidad;
	}

	public void setModelLocalidad(DefaultTableModel modelLocalidad) {
		this.modelLocalidad = modelLocalidad;
	}
	

	public String[] getNombreColumnas() {
		return nombreColumnas;
	}

	public void setNombreColumnas(String[] nombreColumnas) {
		this.nombreColumnas = nombreColumnas;
	}
	
	public JTable getTablaPaises() {
		return tablaPaises;
	}

	public void setTablaPaises(JTable tablaLocalidades) {
		this.tablaPaises = tablaLocalidades;
	}

	public JButton getBtnBorrar() {
		return btnBorrar;
	}

	public void setBtnBorrar(JButton btnBorrar) {
		this.btnBorrar = btnBorrar;
	}

	
	public JButton getBtnAgregarPais() {
		return btnAgregarPais;
	}

	public void setBtnAgregarPais(JButton btnAgregarPais) {
		this.btnAgregarPais = btnAgregarPais;
	}
	
	public JTextField getTextNuevoPais() {
		return textNuevoPais;
	}

	public void setTextNuevoPais(JTextField textNuevoPais) {
		this.textNuevoPais = textNuevoPais;
	}

	public void llenarTabla(List<PaisDTO> PaisesEnTabla) 
	{
		this.getModelLocalidad().setRowCount(0); //Para vaciar la tabla
		this.getModelLocalidad().setColumnCount(0);
		this.getModelLocalidad().setColumnIdentifiers(this.getNombreColumnas());

		for (PaisDTO p : PaisesEnTabla)
		{
			int id = p.getIdPais();
			String nombre = p.getNombre();
			Object[] fila = {id,nombre};
			this.getModelLocalidad().addRow(fila);
		}
		
	}
	
	
	public void cerrar()
	{
		
	}
}
