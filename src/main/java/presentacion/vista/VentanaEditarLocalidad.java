package presentacion.vista;


import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import java.awt.Font;

public class VentanaEditarLocalidad extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtNombre;
	private JButton btnAceptar;
	private static VentanaEditarLocalidad INSTANCE;
	

	public static VentanaEditarLocalidad getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaEditarLocalidad(); 	
			return new VentanaEditarLocalidad();
		}
		else
			return INSTANCE;
	}

	private VentanaEditarLocalidad() 
	{
		super();
		setTitle("Editar Localidad");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 200, 385, 139);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 349, 78);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNombre = new JLabel("Nueva Localidad:");
		lblNombre.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNombre.setBounds(10, 11, 113, 14);
		panel.add(lblNombre);
		
		txtNombre = new JTextField();
		txtNombre.setBounds(133, 8, 206, 20);
		panel.add(txtNombre);
		txtNombre.setColumns(10);
	
		btnAceptar = new JButton("Guardar");
		btnAceptar.setBounds(10, 36, 95, 31);
		panel.add(btnAceptar);
		
		this.setVisible(false);
	}
	
	public void mostrarVentana()
	{
		this.setVisible(true);
	}
	
	public JTextField getTxtNombre() 
	{
		return txtNombre;
	}

	public JButton getBtnAceptar() {
		return btnAceptar;
	}

	public void setBtnAceptar(JButton btnAceptar) {
		this.btnAceptar = btnAceptar;
	}

	public void cerrar()
	{
		this.txtNombre.setText(null);
		this.dispose();
	}
}
