package presentacion.vista;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class VentanaConfiguracionBD extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JButton btnGuardar;
	private static VentanaConfiguracionBD INSTANCE;
	private JTextField textPassword;
	private JTextField textUsuario;
	private JTextField textServidor;
	private JTextField textDriver;
	
	public static VentanaConfiguracionBD getInstance() 
	{
		
		if (INSTANCE == null) 
		{
			INSTANCE = new VentanaConfiguracionBD();
			return new VentanaConfiguracionBD();
		}
		else
			return INSTANCE;
	}

	/**
	 * Create the frame.
	 */
	public VentanaConfiguracionBD() 
	{
		super();
		setTitle("Configuracion BD");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 516, 418);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);		
		
		JPanel panel = new JPanel();
		panel.setBounds(20, 20, 606, 236);
		contentPane.add(panel);
		panel.setLayout(null);

		btnGuardar = new JButton("Ingresar");
		btnGuardar.setBounds(172, 312, 129, 36);
		panel.add(btnGuardar);
		
		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblPassword.setBounds(12, 241, 100, 14);
		panel.add(lblPassword);
		
		textPassword = new JTextField();
		textPassword.setBounds(125, 241, 255, 20);
		panel.add(textPassword);
		textPassword.setColumns(10);
		
		JLabel lblUsuario = new JLabel("Usuario:");
		lblUsuario.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblUsuario.setBounds(12, 197, 100, 16);
		panel.add(lblUsuario);
		
		textUsuario = new JTextField();
		textUsuario.setBounds(125, 194, 255, 22);
		panel.add(textUsuario);
		textUsuario.setColumns(10);
		
		JLabel lblServidor = new JLabel("Servidor/Host:");
		lblServidor.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblServidor.setBounds(12, 146, 109, 16);
		panel.add(lblServidor);
		
		textServidor = new JTextField();
		textServidor.setBounds(125, 143, 254, 22);
		panel.add(textServidor);
		textServidor.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Driver: ");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblNewLabel.setBounds(12, 98, 100, 16);
		panel.add(lblNewLabel);
		
		textDriver = new JTextField();
		textDriver.setBounds(125, 95, 255, 22);
		panel.add(textDriver);
		textDriver.setColumns(10);
		
		JLabel lblPorFavor = new JLabel("Por favor ingrese los datos de la base de datos:");
		lblPorFavor.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblPorFavor.setBounds(56, 13, 386, 72);
		panel.add(lblPorFavor);
		
		this.setVisible(false);
		}
	
	public JButton getBtnGuardar() {
		return btnGuardar;
	}

	public void setBtnGuardar(JButton btnGuardar) {
		this.btnGuardar = btnGuardar;
	}

	
	public JTextField getTextPassword() {
		return textPassword;
	}

	public void setTextPassword(JTextField textPassword) {
		this.textPassword = textPassword;
	}

	public JTextField getTextUsuario() {
		return textUsuario;
	}

	public void setTextUsuario(JTextField textUsuario) {
		this.textUsuario = textUsuario;
	}

	public JTextField getTextServidor() {
		return textServidor;
	}

	public void setTextServidor(JTextField textServidor) {
		this.textServidor = textServidor;
	}

	public JTextField getTextDriver() {
		return textDriver;
	}

	public void setTextDriver(JTextField textDriver) {
		this.textDriver = textDriver;
	}

	public void mostrarVentana() 
		{
			this.setVisible(true);
		}
	
	public void cerrar()
	{
		this.dispose();
	}
}
