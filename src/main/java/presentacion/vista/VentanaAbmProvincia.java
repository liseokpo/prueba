package presentacion.vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import dto.LocalidadDTO;
import dto.PaisDTO;
import dto.ProvinciaDTO;
import dto.TipoContactoDTO;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;

public class VentanaAbmProvincia extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JButton btnBorrar;
	private JButton btnAgregarProvincia;
	private static VentanaAbmProvincia INSTANCE;
	private DefaultTableModel modelProvincia;
	private JTable tablaProvincias;
	private JComboBox<String> comboBoxPais;
	private  String[] nombreColumnas = {"Id","Nombre","idPais"};
	private JTextField textNuevaProvincia;
	
	public static VentanaAbmProvincia getInstance() 
	{
		
		if (INSTANCE == null) 
		{
			INSTANCE = new VentanaAbmProvincia();
			return new VentanaAbmProvincia();
		}
		else
			return INSTANCE;
	}

	/**
	 * Create the frame.
	 */
	public VentanaAbmProvincia() 
	{
		super();
		setTitle("Nueva Provincia");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 687, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);		
		
		JPanel panel = new JPanel();
		panel.setBounds(20, 20, 606, 236);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JScrollPane spTiposContactos = new JScrollPane();
		spTiposContactos.setBounds(10, 11, 390, 182);
		panel.add(spTiposContactos);
		
		modelProvincia = new DefaultTableModel(null,nombreColumnas);
		tablaProvincias = new JTable(modelProvincia);
		
		spTiposContactos.setViewportView(tablaProvincias);
		
		btnBorrar = new JButton("Borrar");
		btnBorrar.setBounds(122, 204, 111, 36);
		panel.add(btnBorrar);
		
		btnAgregarProvincia = new JButton("Agregar Provincia");
		btnAgregarProvincia.setBounds(458, 157, 146, 36);
		panel.add(btnAgregarProvincia);
		
		JLabel lblNuevaProvincia = new JLabel("Nueva Provincia: ");
		lblNuevaProvincia.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNuevaProvincia.setBounds(410, 103, 121, 14);
		panel.add(lblNuevaProvincia);
		
		textNuevaProvincia = new JTextField();
		textNuevaProvincia.setBounds(530, 101, 121, 20);
		panel.add(textNuevaProvincia);
		textNuevaProvincia.setColumns(10);
		
		comboBoxPais = new JComboBox<String>();
		comboBoxPais.setBounds(534, 49, 117, 20);
		panel.add(comboBoxPais);
		
		JLabel lblPaises = new JLabel("Paises:");
		lblPaises.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblPaises.setBounds(410, 51, 78, 14);
		panel.add(lblPaises);
		
		this.setVisible(false);
		}
	
	public void mostrarVentana() 
		{
			this.setVisible(true);
		}
	
	public DefaultTableModel getModelProvincia() {
		return modelProvincia;
	}

	public void setModelProvincia(DefaultTableModel modelProvincia) {
		this.modelProvincia = modelProvincia;
	}
	

	public String[] getNombreColumnas() {
		return nombreColumnas;
	}

	public void setNombreColumnas(String[] nombreColumnas) {
		this.nombreColumnas = nombreColumnas;
	}
	
	public JTable getTablaPaises() {
		return tablaProvincias;
	}

	public void setTablaPaises(JTable tablaProvincias) {
		this.tablaProvincias = tablaProvincias;
	}

	public JButton getBtnBorrar() {
		return btnBorrar;
	}

	public void setBtnBorrar(JButton btnBorrar) {
		this.btnBorrar = btnBorrar;
	}

	
	public JButton getBtnAgregarProvincia() {
		return btnAgregarProvincia;
	}

	public void setBtnAgregarProvincia(JButton btnAgregarProvincia) {
		this.btnAgregarProvincia = btnAgregarProvincia;
	}
	
	public String getComboPais() {
		return  comboBoxPais.getSelectedItem().toString();
	}

	public void setComboBoxPais(JComboBox<String> comboBoxPais) {
		this.comboBoxPais = comboBoxPais;
	}
	
	public JTextField getTextNuevaProvincia() {
		return textNuevaProvincia;
	}

	public void setTextNuevaProvincia(JTextField textNuevaProvincia) {
		this.textNuevaProvincia = textNuevaProvincia;
	}

	public void llenarTabla(List<ProvinciaDTO> provinciasEnTabla) 
	{
		this.getModelProvincia().setRowCount(0); //Para vaciar la tabla
		this.getModelProvincia().setColumnCount(0);
		this.getModelProvincia().setColumnIdentifiers(this.getNombreColumnas());

		for (ProvinciaDTO p : provinciasEnTabla)
		{
			int id = p.getIdProvincia();
			String nombre = p.getNombre();
			int idPais =  p.getIdPais();
			Object[] fila = {id,nombre,idPais};
			this.getModelProvincia().addRow(fila);
		}
		
	}
	
	public void agregarElementoAComboPais(List<PaisDTO> paisesEnTabla)
	{
		comboBoxPais.removeAllItems();
		for (int i = 0; i < paisesEnTabla.size(); i++)
		{
			comboBoxPais.addItem(paisesEnTabla.get(i).getNombre());
		}
	}
	

	public void cerrar()
	{
		
	}
}
