package presentacion.vista;

import java.awt.BorderLayout;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;


import dto.TipoContactoDTO;
import java.awt.Font;


public class VentanaTipoContacto extends JFrame {
	
	private static final long serialVersionUID = 1L;
	private static VentanaTipoContacto INSTANCE;
	private JPanel contentPane;
	private JTextField txtTipo;
	private JButton btnAgregarNuevoTipo;
	private JButton btnBorrar;
	private JButton btnEditar;
	private JTable tablaTiposContactos;
	private DefaultTableModel modelTipoContacto;
	private  String[] nombreColumnas = {"Id","Nombre"};
	private JTextField textNombre;
	
	
	//Singleton
	public static VentanaTipoContacto getInstance() 
	{
		
		if (INSTANCE == null) 
		{
			INSTANCE = new VentanaTipoContacto();
			return new VentanaTipoContacto();
		}
		else
			return INSTANCE;
	}
	
	
	private VentanaTipoContacto()
	{
	super();
	setTitle("Nuevo Tipo Contacto");
	setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	setBounds(100, 500, 647, 300);
	contentPane = new JPanel();
	contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
	contentPane.setLayout(new BorderLayout(0, 0));
	setContentPane(contentPane);
	
	JPanel panel = new JPanel();
	panel.setBounds(20, 20, 606, 236);
	contentPane.add(panel);
	panel.setLayout(null);
	
	JScrollPane spTiposContactos = new JScrollPane();
	spTiposContactos.setBounds(10, 11, 390, 182);
	panel.add(spTiposContactos);
	
	modelTipoContacto = new DefaultTableModel(null,nombreColumnas);
	tablaTiposContactos = new JTable(modelTipoContacto);
	
	spTiposContactos.setViewportView(tablaTiposContactos);
	
	btnEditar = new JButton("Editar");
	btnEditar.setBounds(48, 204, 120, 36);
	panel.add(btnEditar);
	
	btnBorrar = new JButton("Borrar");
	btnBorrar.setBounds(247, 204, 120, 36);
	panel.add(btnBorrar);
	
	btnAgregarNuevoTipo = new JButton("Guardar");
	btnAgregarNuevoTipo.setBounds(410, 63, 105, 30);
	panel.add(btnAgregarNuevoTipo);
	
	textNombre = new JTextField();
	textNombre.setBounds(484, 32, 105, 20);
	panel.add(textNombre);
	textNombre.setColumns(10);
	
	JLabel lblNombre = new JLabel("Nombre:");
	lblNombre.setFont(new Font("Tahoma", Font.BOLD, 12));
	lblNombre.setBounds(410, 35, 64, 14);
	panel.add(lblNombre);
	
	this.setVisible(false);
	}
	
	public void mostrarVentana() 
	{
		this.setVisible(true);
	}

	public JTextField getTxtTipo() {
		return txtTipo;
	}

	public void setTxtTipo(JTextField txtTipo) {
		this.txtTipo = txtTipo;
	}

	public JTextField getTextNombre() {
		return textNombre;
	}

	public void setTextNombre(JTextField textNombre) {
		this.textNombre = textNombre;
	}
	
	public JButton getBtnAgregarNuevoTipo() {
		return btnAgregarNuevoTipo;
	}

	public void setBtnAgregarNuevoTipo(JButton btnAgregarNuevoTipo) {
		this.btnAgregarNuevoTipo = btnAgregarNuevoTipo;
	}
	
	public JButton getBtnBorrar() {
		return btnBorrar;
	}

	public void setBtnBorrar(JButton btnBorrar) {
		this.btnBorrar = btnBorrar;
	}
	
	public JTable getTablaTiposContactos() {
		return tablaTiposContactos;
	}

	public void setTablaTiposContactos(JTable tablaTiposContactos) {
		this.tablaTiposContactos = tablaTiposContactos;
	}
	
	public JButton getBtnEditar() {
		return btnEditar;
	}

	public void setBtnEditar(JButton btnEditar) {
		this.btnEditar = btnEditar;
	}

	public void llenarTabla(List<TipoContactoDTO> tiposContactosEnTabla) {
		this.getModelTipoContacto().setRowCount(0); //Para vaciar la tabla
		this.getModelTipoContacto().setColumnCount(0);
		this.getModelTipoContacto().setColumnIdentifiers(this.getNombreColumnas());

		for (TipoContactoDTO p : tiposContactosEnTabla)
		{
			int id = p.getIdTipoContacto();
			String nombre = p.getNombre();
			Object[] fila = {id,nombre};
			this.getModelTipoContacto().addRow(fila);
		}
		
	}

	public String[] getNombreColumnas() {
		return nombreColumnas;
	}


	public void setNombreColumnas(String[] nombreColumnas) {
		this.nombreColumnas = nombreColumnas;
	}


	public DefaultTableModel getModelTipoContacto() {
		return modelTipoContacto;
	}


	public void setModelTipoContacto(DefaultTableModel modelTipoContacto) {
		this.modelTipoContacto = modelTipoContacto;
	}


	public void cerrar() 
	{
		this.textNombre.setText(null);
		
	}
}
