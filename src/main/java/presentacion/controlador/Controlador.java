package presentacion.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import org.jfree.data.statistics.Regression;

import com.sun.org.apache.bcel.internal.generic.NEWARRAY;

import modelo.Agenda;
import persistencia.conexion.Conexion;
import presentacion.reportes.ReporteAgenda;
import presentacion.vista.VentanaAbmPais;
import presentacion.vista.VentanaAbmProvincia;
import presentacion.vista.VentanaConfiguracionBD;
import presentacion.vista.VentanaCrearLocalidad;
import presentacion.vista.VentanaEditarLocalidad;
import presentacion.vista.VentanaEditarPersona;
import presentacion.vista.VentanaEditarTipoContacto;
import presentacion.vista.VentanaLocalidad;
import presentacion.vista.VentanaPersona;
import presentacion.vista.VentanaTipoContacto;
import presentacion.vista.Vista;
import dto.LocalidadDTO;
import dto.PaisDTO;
import dto.PersonaDTO;
import dto.ProvinciaDTO;
import dto.TipoContactoDTO;

public class Controlador implements ActionListener
{
		private Vista vista;
		private List<PersonaDTO> personasEnTabla;
		private List<TipoContactoDTO> tipoContactosEnTabla;
		private List<LocalidadDTO> localidadesEnTabla;
		private List<PaisDTO> paisesEnTabla;
		private List<ProvinciaDTO> provinciasEnTabla;
		private VentanaPersona ventanaPersona; 
		private VentanaTipoContacto ventanaTipoContacto;
		private VentanaEditarTipoContacto ventanaEditarTipoContacto;
		private VentanaLocalidad ventanaLocalidad;
		private VentanaEditarLocalidad ventanaEditarLocalidad;
		private VentanaEditarPersona ventanaEditarPersona;
		private VentanaAbmPais ventanaAbmPais;
		private VentanaAbmProvincia ventanaAbmProvincia;
		private VentanaCrearLocalidad ventanaCrearLocalidad;
		private VentanaConfiguracionBD ventanaConfiguracionBD;
		private Agenda agenda;
		public Conexion conexion;
		
		
		public Controlador(Vista vista, Agenda agenda)
		{
			this.vista = vista;
			this.vista.getBtnAgregar().addActionListener(a->ventanaAgregarPersona(a));
			this.vista.getBtnBorrar().addActionListener(s->borrarPersona(s));
			this.vista.getBtnReporte().addActionListener(r->mostrarReporte(r));
			this.vista.getBtnNuevoTipoContacto().addActionListener(q->ventanaAgregarTipoContacto(q));
			this.vista.getBtnLocalidadAbm().addActionListener(h->ventanaAgregarLocalidad(h));
			this.ventanaTipoContacto = VentanaTipoContacto.getInstance();
			this.ventanaPersona = VentanaPersona.getInstance();
			this.ventanaEditarTipoContacto = VentanaEditarTipoContacto.getInstance();
			this.ventanaLocalidad = VentanaLocalidad.getInstance();
			this.ventanaEditarLocalidad = VentanaEditarLocalidad.getInstance();
			this.ventanaEditarPersona = VentanaEditarPersona.getInstance();
			this.ventanaAbmPais = VentanaAbmPais.getInstance();
			this.ventanaAbmProvincia = VentanaAbmProvincia.getInstance();
			this.ventanaCrearLocalidad = VentanaCrearLocalidad.getInstance();
			this.ventanaConfiguracionBD = VentanaConfiguracionBD.getInstance();
			this.ventanaEditarPersona.getBtnActualizar().addActionListener(e->editarPersona(e));
			this.ventanaPersona.getBtnAgregarPersona().addActionListener(p->guardarPersona(p));
			this.ventanaTipoContacto.getBtnAgregarNuevoTipo().addActionListener(q->guardarTipoContacto(q));
			this.ventanaLocalidad.getBtnNuevaLocalidad().addActionListener(f->ventanaCrearLocalidad(f));
			this.ventanaTipoContacto.getBtnBorrar().addActionListener(i->borrarTipoContacto(i));
			this.ventanaLocalidad.getBtnBorrar().addActionListener(g->borrarLocalidad(g));
			this.ventanaTipoContacto.getBtnEditar().addActionListener(j->ventanaEditarTipoContacto(j));
			this.ventanaLocalidad.getBtnEditar().addActionListener(d->ventanaEditarLocalidad(d));
			this.ventanaEditarTipoContacto.getBtnAceptar().addActionListener(k->editarTipoContacto(k));
			this.ventanaEditarLocalidad.getBtnAceptar().addActionListener(s->editarLocalidad(s));
			this.vista.getBtnEditar().addActionListener(c->ventanaEditarPersona(c));
			this.ventanaLocalidad.getBtnAgregarPais().addActionListener(u->ventanaAmbPais(u));
			this.ventanaLocalidad.getBtnAgregarProvincia().addActionListener(r->ventanaAbmProvincia(r));
			this.ventanaAbmProvincia.getBtnAgregarProvincia().addActionListener(m->guardarProvincia(m));
			this.ventanaCrearLocalidad.getBtnNuevaLocalidad().addActionListener(b->guardarLocalidad(b));
			this.ventanaCrearLocalidad.getComboBoxPais().addItemListener(n->refrescarProvincias(n));
		//	this.vista.getBtnConfiguracinDeBase().addActionListener(�->ventanaConfiguracionBD(�));
			this.ventanaAbmPais.getBtnAgregarPais().addActionListener(x->guardarPais(x));
			this.ventanaAbmPais.getBtnBorrar().addActionListener(y->borrarPais(y));
			this.ventanaAbmProvincia.getBtnBorrar().addActionListener(v->borrarProvincia(v));
			this.ventanaConfiguracionBD.getBtnGuardar().addActionListener(z->ConectarBD(z));
			this.agenda = agenda;
		}
		


		private void ventanaAbmProvincia(ActionEvent r)
		{
			this.refrescarTablaPais();
			this.refrescarTablaProvincia();
			this.ventanaAbmProvincia.mostrarVentana();
			this.ventanaAbmProvincia.agregarElementoAComboPais(paisesEnTabla);
		}
		
		private void ventanaAmbPais(ActionEvent u) 
		{
			this.ventanaAbmPais.mostrarVentana();
			this.refrescarTablaPais();
		}
		
		private void ventanaCrearLocalidad(ActionEvent f)
		{
			this.ventanaCrearLocalidad.mostrarVentana();
			this.refrescarTablaPais();
			this.refrescarTablaProvincia();
			if(this.ventanaCrearLocalidad.getComboBoxPais().getSelectedItem() == null)
				this.ventanaCrearLocalidad.agregarElementoAComboPais(paisesEnTabla);
		}
		
		private void ventanaAgregarPersona(ActionEvent a) 
		{
			this.ventanaPersona.mostrarVentana();
			this.refrescarTablaTipoContacto();
			this.refrescarTablaLocalidad();
			if(this.ventanaPersona.getComboLocalidades().getSelectedItem() == null && this.ventanaPersona.getComboTipoContacto().getSelectedItem() == null) {
				this.ventanaPersona.AgregarElementosAComboTipoContacto(tipoContactosEnTabla);
				this.ventanaPersona.AgregarElementoAComboLocalidad(localidadesEnTabla);
			}else
				{
				this.ventanaPersona.getComboLocalidades().removeAllItems();
				this.ventanaPersona.getComboTipoContacto().removeAllItems();
				this.ventanaPersona.AgregarElementosAComboTipoContacto(tipoContactosEnTabla);
				this.ventanaPersona.AgregarElementoAComboLocalidad(localidadesEnTabla);
				}
		
		}
		
		private void ventanaAgregarTipoContacto(ActionEvent q)
		{
			this.ventanaTipoContacto.mostrarVentana();
			this.refrescarTablaTipoContacto();
		}
		
		private void ventanaAgregarLocalidad(ActionEvent h)
		{
			this.ventanaLocalidad.mostrarVentana();
			this.refrescarTablaLocalidad();
		}
		
		public void ventanaEditarPersona(ActionEvent c)
		{
			if(this.vista.getTablaPersonas().getSelectedRows().length != 1)
			{
				JOptionPane.showMessageDialog(null, "Usted no seleccion� o seleccion� m�s de una fila");
			}else
			{
				this.ventanaEditarPersona.mostrarVentana();
				this.refrescarTablaLocalidad();
				this.refrescarTablaTipoContacto();
				this.refrescarDatosAEditar();
			}

		}
		
		private void ventanaEditarTipoContacto(ActionEvent j)
		{
			if (this.ventanaTipoContacto.getTablaTiposContactos().getSelectedRows().length != 1 )
			{
				JOptionPane.showMessageDialog(null, "Usted no seleccion� o seleccion� m�s de una fila");
			}
			else
			{
				this.ventanaEditarTipoContacto.mostrarVentana();
			}
		}
		
		private void ventanaEditarLocalidad(ActionEvent d)
		{

			if (this.ventanaLocalidad.getTablaLocalidades().getSelectedRows().length != 1 )
			{
				JOptionPane.showMessageDialog(null, "Usted no seleccion� o seleccion� m�s de una fila");
			}
			else
			{
				this.ventanaEditarLocalidad.mostrarVentana();
			}
		}
		

		private void guardarPersona(ActionEvent p) 
		{
			String nombre = this.ventanaPersona.getTxtNombre().getText();
			String tel = ventanaPersona.getTxtTelefono().getText();
			String email = ventanaPersona.getTxtEmail().getText();
			String tipoContacto = ventanaPersona.getTextComboTipoContacto().toString();
			String fecha = ventanaPersona.getTxtFechaNac().getText();
			String calle = ventanaPersona.getTextFieldCalle().getText();
			String altura = ventanaPersona.getTextFieldAltura().getText();
			String piso = ventanaPersona.getTextFieldPiso().getText();
			String dpto = ventanaPersona.getTextFieldDpto().getText();
			String localidad = ventanaPersona.getTextComboLocalidad().toString();
			String linkedin = ventanaPersona.getTxtLinkedin().getText();
			String cp = ventanaPersona.getTxtCp().getText();
			
			if (!nombre.isEmpty() && !tel.isEmpty() && !email.isEmpty() && !tipoContacto.isEmpty() && !calle.isEmpty() && !altura.isEmpty() && !piso.isEmpty() && !dpto.isEmpty() && !localidad.isEmpty() && !fecha.isEmpty() && !linkedin.isEmpty() && !cp.isEmpty())
			{
				PersonaDTO nuevaPersona = new PersonaDTO(0, nombre, tel, email, tipoContacto, calle, altura, piso,dpto, localidad, fecha, linkedin, cp);
				this.agenda.agregarPersona(nuevaPersona);
				this.refrescarTabla();
				this.ventanaPersona.getComboLocalidades().removeAllItems();
				this.ventanaPersona.cerrar();
			}
			else
			{
				JOptionPane.showMessageDialog(null, "No pueden existir campos vacios");
			}
		}
		
		private void guardarTipoContacto(ActionEvent q)
		{
			String nombre = this.ventanaTipoContacto.getTextNombre().getText();
			TipoContactoDTO nuevoTipo = new TipoContactoDTO(0, nombre);
			if(nombre.isEmpty())
			{
				JOptionPane.showMessageDialog(null, "No puede existir campos vacios");
				this.ventanaTipoContacto.cerrar();
			}
			else
			{
				this.agenda.agregarTipoContacto(nuevoTipo);
				this.ventanaTipoContacto.cerrar();
				this.refrescarTablaTipoContacto();	
			}
		}
		
		public void guardarLocalidad(ActionEvent f)
		{
			String pais = this.ventanaCrearLocalidad.getComboPais().toString();
			String provincia = this.ventanaCrearLocalidad.getComboProvincia().toString();
			String localidad = this.ventanaCrearLocalidad.getTextFieldLocalidad().getText();
			LocalidadDTO nuevaLocalidad = new LocalidadDTO(0, localidad,provincia,pais);
			if (!pais.isEmpty() && !provincia.isEmpty() && !localidad.isEmpty())
			{
				this.agenda.agregarLocalidad(nuevaLocalidad);
				this.ventanaCrearLocalidad.cerrar();
				this.refrescarTablaLocalidad();
				
			}else
			{
				JOptionPane.showMessageDialog(null, "Debe ingresar una localidad");
				this.ventanaCrearLocalidad.cerrar();
			}
		}
		
		public void guardarProvincia(ActionEvent m)
		{
			boolean bandera = false;
			ProvinciaDTO nuevaProvincia = null;
			int idPais = 0;
			String nombre_provincia = this.ventanaAbmProvincia.getTextNuevaProvincia().getText();
			String pais_Seleccionado = ventanaAbmProvincia.getComboPais().toString();
			
			for (int i = 0 ; i<paisesEnTabla.size(); i++)
				{
					if (paisesEnTabla.get(i).getNombre().equals(pais_Seleccionado))
					{
						idPais = paisesEnTabla.get(i).getIdPais();
						break;
					}
						
				}	
				
				for(int i = 0; i< provinciasEnTabla.size(); i++)
				{
					if (provinciasEnTabla.get(i).getIdPais() == idPais && provinciasEnTabla.get(i).getNombre().equals(nombre_provincia))
					{
						JOptionPane.showMessageDialog(null, "Ya existe la provincia para dicho pa�s");
						this.ventanaAbmProvincia.cerrar();
						bandera = true;
						break;			
					}		
				}		
				
				if (!bandera)
				{
					nuevaProvincia = new ProvinciaDTO(0, nombre_provincia, idPais);
					this.agenda.agregarProvincia(nuevaProvincia);
					this.ventanaAbmProvincia.cerrar();
					this.refrescarTablaProvincia();
				}
				
			}
				
		
		public void guardarPais (ActionEvent x)
		{
			String nuevoPais = this.ventanaAbmPais.getTextNuevoPais().getText();
			if (!nuevoPais.isEmpty())
			{
				PaisDTO nuevo_pais = new PaisDTO(0, nuevoPais);
				this.agenda.agregarPais(nuevo_pais);
				this.ventanaAbmPais.cerrar();
				this.refrescarTablaPais();
			}else
			{
				JOptionPane.showMessageDialog(null, "Debe ingresar un pa�s");
			}
		}

		public void borrarPersona(ActionEvent s)
		{
			int[] filasSeleccionadas = this.vista.getTablaPersonas().getSelectedRows();
			for (int fila : filasSeleccionadas)
			{
				this.agenda.borrarPersona(this.personasEnTabla.get(fila));
			}
			
			this.refrescarTabla();
		}
		
		public void borrarTipoContacto(ActionEvent i)
		{
			int[] filasSeleccionadas = this.ventanaTipoContacto.getTablaTiposContactos().getSelectedRows();
			for (int fila : filasSeleccionadas)
			{
				this.agenda.borrarTipoContacto(this.tipoContactosEnTabla.get(fila));
			}
			
			this.refrescarTablaTipoContacto();
		}
		
		public void borrarLocalidad(ActionEvent g)
		{
			int[] filasSeleccionadas = this.ventanaLocalidad.getTablaLocalidades().getSelectedRows();
			for (int fila : filasSeleccionadas)
			{
				this.agenda.borrarLocalidad((this.localidadesEnTabla.get(fila)));
			}
			
			this.refrescarTablaLocalidad();
		}
		
		public void borrarPais(ActionEvent y)
		{
			int[] filasSeleccionadas = this.ventanaAbmPais.getTablaPaises().getSelectedRows();
			for (int fila : filasSeleccionadas)
			{
				this.agenda.borrarPais(this.paisesEnTabla.get(fila));
			}
			
			this.refrescarTablaPais();
		}
		
		public void borrarProvincia(ActionEvent v)
		{
			int[] filasSeleccionadas = this.ventanaAbmProvincia.getTablaPaises().getSelectedRows();
			for (int fila : filasSeleccionadas)
			{
				this.agenda.borrarProvincia(this.provinciasEnTabla.get(fila));
			}
			
			this.refrescarTablaProvincia();
		}
		
		public void editarTipoContacto(ActionEvent j)
		{
			int columnaNombre = 0;
			int[] filasSeleccionadas = this.ventanaTipoContacto.getTablaTiposContactos().getSelectedRows();
			String getElemento = this.ventanaTipoContacto.getModelTipoContacto().getValueAt(filasSeleccionadas[0],columnaNombre).toString();
			TipoContactoDTO contacto_a_actualizar = new TipoContactoDTO(Integer.parseInt(getElemento), this.ventanaEditarTipoContacto.getTxtNombre().getText());
			this.agenda.updateTiposContactos(contacto_a_actualizar);
			this.ventanaEditarTipoContacto.cerrar();
			this.refrescarTablaTipoContacto();
		}
		
		public void editarLocalidad(ActionEvent s)
		{
			int columnaNombre = 0;
			int[] filasSeleccionadas = this.ventanaLocalidad.getTablaLocalidades().getSelectedRows();
			String getElemento = this.ventanaLocalidad.getModelLocalidad().getValueAt(filasSeleccionadas[0],columnaNombre).toString();
			LocalidadDTO localidad_a_actualizar = new LocalidadDTO(Integer.parseInt(getElemento), this.ventanaEditarLocalidad.getTxtNombre().getText(),"","");
			this.agenda.updateLocalidad(localidad_a_actualizar);
			this.ventanaEditarLocalidad.cerrar();
			this.refrescarTablaLocalidad();
		}
		
		public void editarPersona(ActionEvent c)
		{
			//Obtengo el id de la persona seleccionada
			int columnaNombre = 0;
			int[] filasSeleccionadas = this.vista.getTablaPersonas().getSelectedRows();
			String getElemento = this.vista.getModelPersonas().getValueAt(filasSeleccionadas[0],columnaNombre).toString();
			
			String nombre = this.ventanaEditarPersona.getTxtNombre().getText();
			String tel = ventanaEditarPersona.getTxtTelefono().getText();
			String email = ventanaEditarPersona.getTxtEmail().getText();
			String tipoContacto = ventanaEditarPersona.getComboTipoContacto().toString();
			String fecha = ventanaEditarPersona.getTxtFechaNac().getText();
			String calle = ventanaEditarPersona.getTextFieldCalle().getText();
			String altura = ventanaEditarPersona.getTextFieldAltura().getText();
			String piso = ventanaEditarPersona.getTextFieldPiso().getText();
			String dpto = ventanaEditarPersona.getTextFieldDpto().getText();
			String localidad = ventanaEditarPersona.getComboLocalidades().toString();
			String linkedin = ventanaEditarPersona.getTxtLinkedin().getText();
			String cp = ventanaEditarPersona.getTxtCp().getText();
			
			if (!nombre.isEmpty() && !tel.isEmpty() && !email.isEmpty() && !tipoContacto.isEmpty() && !calle.isEmpty() && !altura.isEmpty() && !piso.isEmpty() && !dpto.isEmpty() && !localidad.isEmpty() && !fecha.isEmpty() && !linkedin.isEmpty() && !cp.isEmpty())
			{
				PersonaDTO nuevaPersona = new PersonaDTO(Integer.parseInt(getElemento), nombre, tel, email, tipoContacto, calle, altura, piso,dpto, localidad, fecha, linkedin, cp);
				this.agenda.updatePersona(nuevaPersona);
				this.refrescarTabla();
				this.ventanaEditarPersona.cerrar();
			}
			else
			{
				JOptionPane.showMessageDialog(null, "No pueden existir campos vacios");
			}
		}
		
		private void mostrarReporte(ActionEvent r) {
			ReporteAgenda reporte = new ReporteAgenda(this.ordenarTablaParaReporte());
			reporte.mostrar();	
		}
		
		public void inicializar()
		{
			this.ventanaConfiguracionBD.mostrarVentana();
			//this.regrescarConfiguraci�nBD();
			//this.refrescarTabla();
			//this.vista.show();
		}
		
		private void ConectarBD(ActionEvent z)
		{
			String driver = this.ventanaConfiguracionBD.getTextDriver().getText();
			String host = this.ventanaConfiguracionBD.getTextServidor().getText();
			String usuario = this.ventanaConfiguracionBD.getTextUsuario().getText();
			String password = this.ventanaConfiguracionBD.getTextPassword().getText();
	
			
		
				conexion = new Conexion(driver, host, usuario, password);
				if (conexion.getSQLConexion() != null)
				{
					this.ventanaConfiguracionBD.cerrar();
					this.vista.show();
					this.refrescarTabla();
				}else
				{
					JOptionPane.showMessageDialog(null, "Por favor verificar los datos ingresados");
				}
				
		}
		
		
		
		private void refrescarTabla()
		{
			this.personasEnTabla = agenda.obtenerPersonas();
			this.vista.llenarTabla(this.personasEnTabla);
		}
		
		public void refrescarTablaTipoContacto()
		{
			this.tipoContactosEnTabla = agenda.obtenerTipoContactos();
			this.ventanaTipoContacto.llenarTabla(this.tipoContactosEnTabla);
		}
		
		public void refrescarTablaLocalidad()
		{
			this.localidadesEnTabla = agenda.obtenerLocalidad();
			this.ventanaLocalidad.llenarTabla(this.localidadesEnTabla);
		}
		
		public void refrescarTablaPais()
		{
			this.paisesEnTabla = agenda.obtenerPais();
			this.ventanaAbmPais.llenarTabla(this.paisesEnTabla);
		}
		
		public void refrescarTablaProvincia()
		{
			this.provinciasEnTabla = agenda.obtenerProvincia();
			this.ventanaAbmProvincia.llenarTabla(provinciasEnTabla);
		}
		
		
		public void refrescarProvincias(ItemEvent n)
		{
			String pais_seleccionado = this.ventanaCrearLocalidad.getComboPais().toString();
			for (int i = 0 ; i<paisesEnTabla.size(); i++)
				{
					if (paisesEnTabla.get(i).getNombre().equals(pais_seleccionado))
					{
						
						ProvinciaDTO new_provincia = new ProvinciaDTO(paisesEnTabla.get(i).getIdPais(), "",0);
						List<String> Provincias_a_mostrar = this.agenda.FiltrarProvincias(new_provincia);
						this.ventanaCrearLocalidad.agregarElementoAComboProvincia(Provincias_a_mostrar);
					}
				}	
 
		}
		
		public void refrescarDatosAEditar()
		{
			//fila seleccionada
			int filaSeleccionada = this.vista.getTablaPersonas().getSelectedRow();
			//guardamos las posiciones de las columnas
			int posicionColumnaNombre= 1;
			int posicionColumnaTelefono = 2;
			int posicionColumnaEmail = 3;
			int posicionColumnaFechaNac = 4;
			int posicionColumnCalle = 6;
			int posicionColumnaAltura = 7;
			int posicionColumnaPiso = 8;
			int posicionColumnaDpto = 9;
			int posicionColumnaLinkedin = 11;
			int posicionColumnaCp = 12;
			//Carga los comboBox
			this.ventanaEditarPersona.AgregarElementosAComboTipoContacto(tipoContactosEnTabla);
			this.ventanaEditarPersona.AgregarElementoAComboLocalidad(localidadesEnTabla);
			//setea los campos de la fila seleccionada
			this.ventanaEditarPersona.getTxtNombre().setText(this.vista.getTablaPersonas().getValueAt(filaSeleccionada, posicionColumnaNombre).toString());
			this.ventanaEditarPersona.getTxtTelefono().setText(this.vista.getTablaPersonas().getValueAt(filaSeleccionada, posicionColumnaTelefono).toString());		
			this.ventanaEditarPersona.getTxtEmail().setText(this.vista.getTablaPersonas().getValueAt(filaSeleccionada, posicionColumnaEmail).toString());
			this.ventanaEditarPersona.getTxtFechaNac().setText(this.vista.getTablaPersonas().getValueAt(filaSeleccionada, posicionColumnaFechaNac).toString());
			this.ventanaEditarPersona.getTextFieldCalle().setText(this.vista.getTablaPersonas().getValueAt(filaSeleccionada, posicionColumnCalle).toString());
			this.ventanaEditarPersona.getTextFieldAltura().setText(this.vista.getTablaPersonas().getValueAt(filaSeleccionada, posicionColumnaAltura).toString());
			this.ventanaEditarPersona.getTextFieldPiso().setText(this.vista.getTablaPersonas().getValueAt(filaSeleccionada, posicionColumnaPiso).toString());
			this.ventanaEditarPersona.getTextFieldDpto().setText(this.vista.getTablaPersonas().getValueAt(filaSeleccionada, posicionColumnaDpto).toString());
			this.ventanaEditarPersona.getTxtLinkedin().setText(this.vista.getTablaPersonas().getValueAt(filaSeleccionada, posicionColumnaLinkedin).toString());
			this.ventanaEditarPersona.getTxtCp().setText(this.vista.getTablaPersonas().getValueAt(filaSeleccionada, posicionColumnaCp).toString());
				
		}
		
		public ArrayList<PersonaDTO> ordenarTablaParaReporte()
		{
			ArrayList<PersonaDTO> contactosOrdenadas = new ArrayList<PersonaDTO>();
			for (int i = 0; i< this.vista.getTablaPersonas().getRowCount(); i++) 
			{
				PersonaDTO contacto = new PersonaDTO(0, null, null, null, null, null, null, null, null, null, null, null, null);
				contacto.setIdPersona((int) this.vista.getTablaPersonas().getValueAt(i, 0));
				contacto.setNombre( (String) this.vista.getTablaPersonas().getValueAt(i, 1));
				contacto.setTelefono( (String) this.vista.getTablaPersonas().getValueAt(i, 2));
				contacto.setEmail( (String) this.vista.getTablaPersonas().getValueAt(i, 3));
				contacto.setTipoContacto( (String) this.vista.getTablaPersonas().getValueAt(i, 4));
				contacto.setCalle( (String) this.vista.getTablaPersonas().getValueAt(i, 5));
				contacto.setAltura( (String) this.vista.getTablaPersonas().getValueAt(i, 6));
				contacto.setPiso( (String) this.vista.getTablaPersonas().getValueAt(i, 7));
				contacto.setDpto( (String) this.vista.getTablaPersonas().getValueAt(i, 8));
				contacto.setLocalidad( (String) this.vista.getTablaPersonas().getValueAt(i, 9));
				contacto.setFecha( (String) this.vista.getTablaPersonas().getValueAt(i, 10));
				contacto.setLinkedin( (String) this.vista.getTablaPersonas().getValueAt(i, 11));
				contacto.setCp( (String) this.vista.getTablaPersonas().getValueAt(i, 12));
				contactosOrdenadas.add(contacto);
			}
			
			return contactosOrdenadas;
		}
		

		@Override
		public void actionPerformed(ActionEvent e) { }
		
}
