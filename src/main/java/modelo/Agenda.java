package modelo;

import java.util.List;

import dto.LocalidadDTO;
import dto.PaisDTO;
import dto.PersonaDTO;
import dto.ProvinciaDTO;
import dto.TipoContactoDTO;
import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.LocalidadDAO;
import persistencia.dao.interfaz.PaisDAO;
import persistencia.dao.interfaz.PersonaDAO;
import persistencia.dao.interfaz.ProvinciaDAO;
import persistencia.dao.interfaz.TipoContactoDAO;


public class Agenda 
{
	private PersonaDAO persona;	
	private TipoContactoDAO tipoContacto;
	private LocalidadDAO localidad;
	private PaisDAO pais;
	private ProvinciaDAO provincia;
	
	public Agenda(DAOAbstractFactory metodo_persistencia)
	{
		this.persona = metodo_persistencia.createPersonaDAO();
		this.tipoContacto = metodo_persistencia.createTipoContactoDAO();
		this.localidad = metodo_persistencia.createLocalidadDAO();
		this.pais = metodo_persistencia.createPaisDAO();
		this.provincia = metodo_persistencia.createProvinciaDAO();
	}
	
	public void agregarPersona(PersonaDTO nuevaPersona)
	{
		this.persona.insert(nuevaPersona);
	}
	
	public void borrarPersona(PersonaDTO persona_a_eliminar) 
	{
		this.persona.delete(persona_a_eliminar);
	}
	
	public void updatePersona(PersonaDTO persona_a_actualizar)
	{
		this.persona.update(persona_a_actualizar);
	}
	
	public List<PersonaDTO> obtenerPersonas()
	{
		return this.persona.readAll();		
	}
		
	public void agregarTipoContacto(TipoContactoDTO nuevoTipoContacto)
	{
		this.tipoContacto.insert(nuevoTipoContacto);
	}
	
	public void borrarTipoContacto(TipoContactoDTO tipoContacto_a_eliminar)
	{
		this.tipoContacto.delete(tipoContacto_a_eliminar);
	}
	
	public void updateTiposContactos(TipoContactoDTO tipoContacto_a_actualizar)
	{
		this.tipoContacto.update(tipoContacto_a_actualizar);	
	}
	
	public List<TipoContactoDTO> obtenerTipoContactos()
	{
		return this.tipoContacto.readAll();		
	}
	
	public void agregarLocalidad(LocalidadDTO localidad)
	{
		this.localidad.insert(localidad);
	}
	
	public void borrarLocalidad(LocalidadDTO localidad)
	{
		this.localidad.delete(localidad);
	}
	
	public void updateLocalidad(LocalidadDTO localidad)
	{
		this.localidad.update(localidad);
	}
	
	public List<LocalidadDTO> obtenerLocalidad()
	{
		return this.localidad.readAll();
	}
	
	public void agregarPais(PaisDTO pais)
	{
		this.pais.insert(pais);
	}
	
	public void borrarPais(PaisDTO pais)
	{
		this.pais.delete(pais);
	}
	
	public List<PaisDTO> obtenerPais()
	{
		return this.pais.readAll();
	}
	
	public void agregarProvincia(ProvinciaDTO provincia)
	{
		this.provincia.insert(provincia);
	}
	
	public void borrarProvincia(ProvinciaDTO provincia)
	{
		this.provincia.delete(provincia);
	}
	
	public List<ProvinciaDTO> obtenerProvincia()
	{
		return this.provincia.readAll();
	}
	
	public List<String> FiltrarProvincias(ProvinciaDTO provincia)
	{
		return this.provincia.readFilter(provincia);
	}
}
