package dto;

public class ProvinciaDTO {

		int idProvincia;
		String nombre;
		int idPais;
		
		public ProvinciaDTO(int idProvincia, String nombre, int idPais) {
				
		this.idProvincia = idProvincia;
		this.nombre = nombre;
		this.idPais = idPais;
		
		}

		public int getIdProvincia() {
			return idProvincia;
		}

		public void setIdProvincia(int idProvincia) {
			this.idProvincia = idProvincia;
		}

		public String getNombre() {
			return nombre;
		}

		public void setNombre(String nombre) {
			this.nombre = nombre;
		}

		public int getIdPais() {
			return idPais;
		}

		public void setIdPais(int idPais) {
			this.idPais = idPais;
		}
		
		
}
