package persistencia.conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.apache.log4j.Logger;

import presentacion.vista.VentanaConfiguracionBD;

public class Conexion 
{
	public static Conexion instancia;
	private Connection connection;
	private static String driver = null;
	private String nombreBD = null;
	private static String host = null;
	private static String usuario = null;
	private static String password = "";
	private Logger log = Logger.getLogger(Conexion.class);	
	
	public Conexion(String driver, String host, String usuario, String password)
	{
		this.setDriver(driver);
		this.setHost(host);
		this.setUsuario(usuario);
		this.setPassword(password);
		
		try
		{
			Class.forName(driver); // quitar si no es necesario
			this.connection = DriverManager.getConnection(host,usuario,password);
			this.connection.setAutoCommit(false);
			log.info("Conexión exitosa");
		}
		catch(Exception e)
		{
			log.error("Conexión fallida", e);
		}	
		
	}
		
	public static Conexion getConexion()   
	{								
		if(instancia == null)
		{
			instancia = new Conexion(driver, host, usuario, password);
		}
		return instancia;
	}

	public Connection getSQLConexion() 
	{
		return this.connection;
	}
	
	public void cerrarConexion()
	{
		try 
		{
			this.connection.close();
			log.info("Conexion cerrada");
		}
		catch (SQLException e) 
		{
			log.error("Error al cerrar la conexión!", e);
		}
		instancia = null;
	}

	public String getDriver() {
		return driver;
	}


	public void setDriver(String driver) {
		Conexion.driver = driver;
	}


	public String getNombreBD() {
		return nombreBD;
	}


	public void setNombreBD(String nombreBD) {
		this.nombreBD = nombreBD;
	}


	public String getHost() {
		return host;
	}


	public void setHost(String host) {
		Conexion.host = host;
	}


	public String getUsuario() {
		return usuario;
	}


	public void setUsuario(String usuario) {
		Conexion.usuario = usuario;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		Conexion.password = password;
	}
	
	
}
