package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.PaisDTO;
import dto.ProvinciaDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.ProvinciaDAO;

public class ProvinciaDAOSQL implements ProvinciaDAO{
	
	private static final String readall = "SELECT * FROM provincia";
	private static final String delete = "DELETE FROM provincia WHERE idProvincia = ?";
	private static final String insert = "INSERT INTO provincia(idProvincia, nombre, idPais) VALUES(?, ?, ?)";	
	private static final String FiltroProvincia = "SELECT provincia.Nombre FROM pais,provincia where provincia.idPais = ? and pais.idPais = ?";

	
	
	@Override
	public List<String> readFilter(ProvinciaDTO provincia) {
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		List<String> paises = new ArrayList<String>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(FiltroProvincia);
			statement.setInt(1, provincia.getIdProvincia());
			statement.setInt(2, provincia.getIdProvincia());
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				paises.add(resultSet.getString("Nombre"));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return paises;
	}
	
	@Override
	public boolean insert(ProvinciaDTO provincia) 
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try
		{
			statement = conexion.prepareStatement(insert);
			statement.setInt(1, provincia.getIdProvincia());
			statement.setString(2, provincia.getNombre());
			statement.setInt(3, provincia.getIdPais());
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isInsertExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		return isInsertExitoso;
    }
	
	@Override
	public boolean delete(ProvinciaDTO provincia) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isdeleteExitoso = false;
		try 
		{
			statement = conexion.prepareStatement(delete);
			statement.setString(1, Integer.toString(provincia.getIdProvincia()));
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isdeleteExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return isdeleteExitoso;
	}

	@Override
	public boolean update(ProvinciaDTO provincia) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<ProvinciaDTO> readAll() {
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<ProvinciaDTO> paises = new ArrayList<ProvinciaDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				paises.add(getProvinciaDTO(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return paises;
	}
	
	private ProvinciaDTO getProvinciaDTO(ResultSet resultSet) throws SQLException
	{
		int id = resultSet.getInt("idProvincia");
		String nombre = resultSet.getString("Nombre");
		int idPais = resultSet.getInt("idPais");
		return new ProvinciaDTO(id, nombre,idPais);
	}

}
