package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.PersonaDAO;
import dto.LocalidadDTO;
import dto.PersonaDTO;

public class PersonaDAOSQL implements PersonaDAO
{
	private static final String insert = "INSERT INTO personas(idPersona, nombre, telefono, email, tipoContacto, calle, altura, piso, dpto, localidad, fecha_nac, linkedin, cp) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	private static final String update = "UPDATE personas SET nombre = ?, telefono = ?, email = ?, tipoContacto = ?, calle = ?, altura = ?, piso = ?, dpto = ?, localidad = ?, fecha_nac = ?, linkedin = ?, cp = ?  WHERE idPersona = ?";
	private static final String delete = "DELETE FROM personas WHERE idPersona = ?";
	private static final String readall = "SELECT * FROM personas";
	//private static final String agruparPorCp = "SELECT cp ,COUNT(*) as cantidad_personas FROM `personas` GROUP BY cp";
		
	public boolean insert(PersonaDTO persona)
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try
		{
			statement = conexion.prepareStatement(insert);
			statement.setInt(1, persona.getIdPersona());
			statement.setString(2, persona.getNombre());
			statement.setString(3, persona.getTelefono());
			statement.setString(4, persona.getEmail());
			statement.setString(5, persona.getTipoContacto());
			statement.setString(6, persona.getCalle());
			statement.setString(7, persona.getAltura());
			statement.setString(8, persona.getPiso());
			statement.setString(9, persona.getDpto());
			statement.setString(10, persona.getLocalidad());
			statement.setString(11, persona.getFecha());
			statement.setString(12, persona.getLinkedin());
			statement.setString(13, persona.getCp());
			
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isInsertExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		
		return isInsertExitoso;
	}
	
	public boolean delete(PersonaDTO persona_a_eliminar)
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isdeleteExitoso = false;
		try 
		{
			statement = conexion.prepareStatement(delete);
			statement.setString(1, Integer.toString(persona_a_eliminar.getIdPersona()));
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isdeleteExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return isdeleteExitoso;
	}
	
	public List<PersonaDTO> readAll()
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<PersonaDTO> personas = new ArrayList<PersonaDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				personas.add(getPersonaDTO(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return personas;
	}
	
	@Override
	public boolean update(PersonaDTO persona_a_actualizar) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isUpdateExitoso = false;
		try 
		{
			statement = conexion.prepareStatement(update);
			statement.setString(1, persona_a_actualizar.getNombre());
			statement.setString(2, persona_a_actualizar.getTelefono());
			statement.setString(3, persona_a_actualizar.getEmail());
			statement.setString(4, persona_a_actualizar.getTipoContacto());
			statement.setString(5, persona_a_actualizar.getCalle());
			statement.setString(6, persona_a_actualizar.getAltura());
			statement.setString(7, persona_a_actualizar.getPiso());
			statement.setString(8, persona_a_actualizar.getDpto());
			statement.setString(9, persona_a_actualizar.getLocalidad());
			statement.setString(10, persona_a_actualizar.getFecha());
			statement.setString(11, persona_a_actualizar.getLinkedin());
			statement.setString(12, persona_a_actualizar.getCp());
			statement.setString(13, String.valueOf(persona_a_actualizar.getIdPersona()));
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isUpdateExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return isUpdateExitoso;
	}
	
	//Devuelve nuevo objeto con los datos de la BD
	private PersonaDTO getPersonaDTO(ResultSet resultSet) throws SQLException
	{
		int id = resultSet.getInt("idPersona");
		String nombre = resultSet.getString("Nombre");
		String tel = resultSet.getString("Telefono");
		String email = resultSet.getString("Email");
		String tipoContacto = resultSet.getString("TipoContacto");
		String fecha = resultSet.getString("Fecha_nac");
		String calle = resultSet.getString("calle");
		String altura = resultSet.getString("altura");
		String piso = resultSet.getString("piso");
		String dpto = resultSet.getString("dpto");
		String localidad = resultSet.getString("localidad");
		String linkedin = resultSet.getString("linkedin");
		String cp = resultSet.getString("cp");
		return new PersonaDTO(id, nombre, tel, email, tipoContacto, calle, altura, piso, dpto, localidad,fecha, linkedin, cp);
	}

}
