package persistencia.dao.interfaz;

import java.util.List;

import dto.TipoContactoDTO;

public interface TipoContactoDAO {

	public boolean insert(TipoContactoDTO contacto);
	
	public boolean delete(TipoContactoDTO contacto);
	
	public boolean update(TipoContactoDTO contacto);
	
	public List<TipoContactoDTO> readAll();
}
