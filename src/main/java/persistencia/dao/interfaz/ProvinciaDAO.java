package persistencia.dao.interfaz;


import java.util.List;

import dto.ProvinciaDTO;

public interface ProvinciaDAO {

	public boolean insert(ProvinciaDTO provincia);
	
	public boolean delete(ProvinciaDTO provincia);
	
	public boolean update(ProvinciaDTO provincia);
	
	public List<String> readFilter(ProvinciaDTO provincia);
	
	public List<ProvinciaDTO> readAll();
}
