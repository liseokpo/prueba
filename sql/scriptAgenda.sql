CREATE TABLE `tipoContacto` (
`idTipoContacto` int (10) NOT NULL AUTO_INCREMENT, 
`Nombre` varchar(20), 
PRIMARY KEY (`idTipoContacto`));

CREATE TABLE `personas` (
 `idPersona` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) NOT NULL,
  `Telefono` varchar(20) NOT NULL,
  `Email` varchar(150) NOT NULL,
  `tipoContacto` varchar(20) NOT NULL,
  `calle` varchar(30),
  `altura` int(10),
  `piso` varchar(10),
  `dpto` varchar(10),
  `localidad` varchar(50), 
  `Fecha_nac` date NOT NULL,
  `linkedin` varchar(130),
  `cp` varchar(50),
    PRIMARY KEY (`idPersona`)
);

CREATE TABLE `localidades` (
`idLocalidad` int (10) NOT NULL AUTO_INCREMENT, 
`Nombre` varchar(50), 
`Provincia` varchar(50), 
`Pais` varchar(50), 
PRIMARY KEY (`idLocalidad`));

CREATE TABLE `pais` (
`idPais` int (100) NOT NULL AUTO_INCREMENT, 
`Nombre` varchar(100), 
PRIMARY KEY (`idPais`));


CREATE TABLE `provincia` (
`idProvincia` int (10) NOT NULL AUTO_INCREMENT, 
`Nombre` varchar(100), 
`idPais` int(100),
 PRIMARY KEY (`idProvincia`));

ALTER TABLE `provincia` ADD INDEX(`idPais`);

LTER TABLE `provincia`
  ADD CONSTRAINT `provincia_ibfk_1` FOREIGN KEY (`idPais`) REFERENCES `pais` (`idPais`) ON DELETE CASCADE ON UPDATE CASCADE;






